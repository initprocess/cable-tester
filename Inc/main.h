/**
  ******************************************************************************
  * File Name          : main.hpp
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H
  /* Includes ------------------------------------------------------------------*/

/* Includes ------------------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define C1_Pin GPIO_PIN_13
#define C1_GPIO_Port GPIOC
#define C20_Pin GPIO_PIN_14
#define C20_GPIO_Port GPIOC
#define C2_Pin GPIO_PIN_15
#define C2_GPIO_Port GPIOC
#define C21_Pin GPIO_PIN_0
#define C21_GPIO_Port GPIOC
#define C3_Pin GPIO_PIN_1
#define C3_GPIO_Port GPIOC
#define C22_Pin GPIO_PIN_2
#define C22_GPIO_Port GPIOC
#define C4_Pin GPIO_PIN_3
#define C4_GPIO_Port GPIOC
#define C23_Pin GPIO_PIN_0
#define C23_GPIO_Port GPIOA
#define C5_Pin GPIO_PIN_1
#define C5_GPIO_Port GPIOA
#define C24_Pin GPIO_PIN_2
#define C24_GPIO_Port GPIOA
#define C6_Pin GPIO_PIN_3
#define C6_GPIO_Port GPIOA
#define C25_Pin GPIO_PIN_4
#define C25_GPIO_Port GPIOA
#define C7_Pin GPIO_PIN_5
#define C7_GPIO_Port GPIOA
#define C26_Pin GPIO_PIN_6
#define C26_GPIO_Port GPIOA
#define C8_Pin GPIO_PIN_7
#define C8_GPIO_Port GPIOA
#define C27_Pin GPIO_PIN_4
#define C27_GPIO_Port GPIOC
#define C9_Pin GPIO_PIN_5
#define C9_GPIO_Port GPIOC
#define C28_Pin GPIO_PIN_0
#define C28_GPIO_Port GPIOB
#define C10_Pin GPIO_PIN_1
#define C10_GPIO_Port GPIOB
#define C29_Pin GPIO_PIN_2
#define C29_GPIO_Port GPIOB
#define C11_Pin GPIO_PIN_10
#define C11_GPIO_Port GPIOB
#define C30_Pin GPIO_PIN_11
#define C30_GPIO_Port GPIOB
#define C12_Pin GPIO_PIN_12
#define C12_GPIO_Port GPIOB
#define C31_Pin GPIO_PIN_13
#define C31_GPIO_Port GPIOB
#define C13_Pin GPIO_PIN_14
#define C13_GPIO_Port GPIOB
#define C32_Pin GPIO_PIN_15
#define C32_GPIO_Port GPIOB
#define C14_Pin GPIO_PIN_6
#define C14_GPIO_Port GPIOC
#define C33_Pin GPIO_PIN_7
#define C33_GPIO_Port GPIOC
#define C15_Pin GPIO_PIN_8
#define C15_GPIO_Port GPIOC
#define C34_Pin GPIO_PIN_9
#define C34_GPIO_Port GPIOC
#define C16_Pin GPIO_PIN_8
#define C16_GPIO_Port GPIOA
#define C35_Pin GPIO_PIN_9
#define C35_GPIO_Port GPIOA
#define C17_Pin GPIO_PIN_10
#define C17_GPIO_Port GPIOA
#define C36_Pin GPIO_PIN_11
#define C36_GPIO_Port GPIOA
#define C18_Pin GPIO_PIN_12
#define C18_GPIO_Port GPIOA
#define C37_Pin GPIO_PIN_15
#define C37_GPIO_Port GPIOA
#define C19_Pin GPIO_PIN_10
#define C19_GPIO_Port GPIOC
#define K1_Pin GPIO_PIN_11
#define K1_GPIO_Port GPIOC
#define K2_Pin GPIO_PIN_12
#define K2_GPIO_Port GPIOC
#define K3_Pin GPIO_PIN_2
#define K3_GPIO_Port GPIOD
#define DC_Pin GPIO_PIN_6
#define DC_GPIO_Port GPIOB
#define CS_Pin GPIO_PIN_7
#define CS_GPIO_Port GPIOB
#define RST_Pin GPIO_PIN_8
#define RST_GPIO_Port GPIOB

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */
int sensed;
/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

#endif /* __MAIN_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
